//
//  NYCTableViewCell.swift
//  NYC-HighSchools
//
//  Created by Azar Shamseh on 2023-07-18.
//

import UIKit

class NYCTableViewCell: UITableViewCell {

//    @IBOutlet weak var testScoreLbl: UILabel!
    @IBOutlet weak var schoolNameLbl: UILabel!
    @IBOutlet weak var testScoreLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        testScoreLbl.numberOfLines = 0
        schoolNameLbl.numberOfLines = 0
        schoolNameLbl.textAlignment = .center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
