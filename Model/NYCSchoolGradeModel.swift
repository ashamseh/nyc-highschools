//
//  NYCSchoolGradeModel.swift
//  NYC-HighSchools
//
//  Created by Azar Shamseh on 2023-07-18.
//

import Foundation

struct NYCSchoolGradeModel: Decodable {
    var school_name: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    
}
