//
//  ViewController.swift
//  NYC-HighSchools
//
//  Created by Azar Shamseh on 2023-07-17.
//

import UIKit

class ViewController: UIViewController {

    var schoolVm = NYCSchoolViewModel()
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.schoolVm.bindVM(vm: self)
        self.myTableView.delegate = self
        self.myTableView.dataSource = self
        self.myTableView.register(UINib(nibName: "NYCTableViewCell", bundle: nil), forCellReuseIdentifier: "NYCTableViewCell")
        schoolVm.getData()
    }


}


extension ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return schoolVm.getCount()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.myTableView.dequeueReusableCell(withIdentifier: "NYCTableViewCell") as! NYCTableViewCell
        cell.schoolNameLbl.text = schoolVm.getSchoolName(index: indexPath.row)
        cell.testScoreLbl.text = schoolVm.getAverageSatScore(index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(70)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = self.myTableView.dequeueReusableCell(withIdentifier: "NYCTableViewCell") as! NYCTableViewCell
        cell.frame.size.width = CGFloat(340)
        cell.schoolNameLbl.text = schoolVm.getSchoolName(index: indexPath.row)
        cell.testScoreLbl.text = schoolVm.getAverageSatScore(index: indexPath.row)
        let displayVC: NYCScoreViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NYCScoreViewController") as! NYCScoreViewController

        displayVC.schoolName = (schoolVm.getSchoolName(index: indexPath.row))
        displayVC.satScore = "Score: \(schoolVm.getAverageSatScore(index: indexPath.row))"
        //self.present(displayVC, animated: false)
        self.navigationController?.pushViewController(displayVC, animated: true)
        
    }
}
