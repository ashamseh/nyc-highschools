//
//  NYCScoreViewController.swift
//  NYC-HighSchools
//
//  Created by Azar Shamseh on 2023-07-18.
//

import UIKit

class NYCScoreViewController: UIViewController {

    var schoolName: String?
    var satScore: String?
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        schoolNameLabel.numberOfLines = 0
        schoolNameLabel.text = schoolName
        scoreLabel.text = satScore
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
