//
//  NYCApiManager.swift
//  NYC-HighSchools
//
//  Created by Azar Shamseh on 2023-07-18.
//

import Foundation

class NYCApiManager {
    func getData(url: String, completionHandler: @escaping ([NYCSchoolGradeModel])-> Void) {
        URLSession.shared.dataTask(with: URL(string: url)!) { data, response, error in
            do {
            guard let jsonData = data else {return }
                guard let model: [NYCSchoolGradeModel] = try? JSONDecoder().decode([NYCSchoolGradeModel].self, from: jsonData) else {
                    fatalError("model not decoded properly")
                    return
                }
                completionHandler(model)
            }
            catch {
                fatalError("\(error)")
            }
        }.resume()
    }
}
